<!--
This file is used to delete a record from database
Copy this file in /var/www/html and open run http://localhost/delete.php
-->
<html>
<body>

<?php
$sno = $_GET[sno];// get the id value from url parameters

$servername = "localhost";// sql server name
$username = "root";// sql username
$password = "student";// sql password
$dbname  = "SPS";// database name

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

if($_GET["mode"] == "delete"){

$sqldelete = "DELETE FROM S WHERE sno='$sno'";//delete statement
$delete = $conn->query($sqldelete);//execute the query
if($delete)
 { 
  echo "Record deleted successfully!";
 }
}

//Below is the code to show the list of records
$sql = "SELECT * FROM S";// embed a select statement
$result = $conn->query($sql);// get result

if($result->num_rows > 0){// check for number of rows; if there are records, build html table
 echo "<table style='border: solid 1px black;'>
	<tr>
	    <th> sno</th>
	    <th>Name</th>
	    <th>State</th>
	    <th>Delete</th>
	</tr>";
}

while ($row = $result -> fetch_assoc()){// store the result in an array; then put them in html table one by one
	echo '<tr>
		<td>'.$row['sno'].'</td>
		<td>'.$row['sname'].'</td>
		<td>'.$row['state'].'</td>

<!-- below, creates a hyperlink (Delete) and change the mode to "delete". Please note that the link is redirected to the same page (href="delete.php"). -->
		<td> <a href="delete.php?sno='.$row['sno'].'&mode=delete">Delete </a></td>
	      </tr>';
}
 echo "</table>";

?>
</body>
</html>
